package Logic;
import Model.Client;
import Model.Product;
import DAO.WarehouseDAO;
import Main.MyException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Clasa care are rol de validare a datelor si trimirea lor la clasa WarehouseDAO ca sa fie introduse in baza de date
 * variabilele instanta sunt folosite pentru crearea orderului
 */
public class WarehouseLogic {
     private static Client loggedClient;
     private static ArrayList<Product> products=new ArrayList<Product>();
     private static ArrayList<Product> updateQuantity = new ArrayList<Product>();

    /**
     *
     * @return transmite la interfata toti clientii din baza de date
     */
    public static ArrayList<Client> getClients() { return WarehouseDAO.selectAll("clients"); }

    /**
     *
     * @param name Numele clientului de introdus
     * @param email emailul
     * @param age si varsta
     * @throws MyException arunca o exceptie daca una dintre ele este eronata
     */
    public static void insertClient(String name,String email,String age) throws MyException {

            if(emailValidator(email)==false) throw new MyException("email");
            try {
                if (AgeValidator(Integer.parseInt(age)) == false) throw new MyException("age");
            }
            catch (NumberFormatException e)
            {
                throw new MyException("age");
            }
            ArrayList<String> a=new ArrayList<String>();a.add(name);a.add(email);a.add(age);
            WarehouseDAO.insert("clients",a);

     }

    /**
     *
     * @param idclients id clientului pentru care se face comanda
     * @param idproducts id produsului pentru care se face comanda
     * @param quantity si cantitate produslui pentri care se face comanda
     *                 Creaaza o comanda in baza de date
     */
     public static void insertOrderRecord(String idclients,String idproducts,String quantity)
     {
         ArrayList<String> a=new ArrayList<String>();a.add(idclients);a.add(idproducts);a.add(quantity);
         WarehouseDAO.insert("order_record",a);
     }

    /**
     * Insereaza un numar pentru un Order de exemplu 1 care la urma va fii folosit in Order Record ca sa poate fii puse mai multe produse
     */
    public static void insertOrder()
     {
         ArrayList<String> a=new ArrayList<String>();a.add("Order Number:"+(WarehouseDAO.getMaxId("idorders","orders")+1));
         WarehouseDAO.insert("orders",a);
     }

    /**
     *
     * @param id id clientului de sters
     * @throws MyException daca ID ul este introdus gresit se arunca o exceptie la interfata
     */
    public static void deleteClient(String id) throws MyException {
        try {
            Integer.parseInt(id);
            if(WarehouseDAO.delete(id,"clients")==false) throw new MyException("id");
        }
        catch (NumberFormatException e)
        {
            throw new MyException("id");
        }


    }

    /**
     *
     * @param age varsta care trebuie validata
     * @return returneaza true daca e valid si false in caz contrar
     */
    private static boolean AgeValidator(Integer age)
    {
        if (age>0) return true;
        else return false;
    }

    /**
     *
     * @param quan cantitatea care trebuie validata
     * @return returneaza true daca e valid false in caz contrar
     */
    private static boolean quantityValidator(Integer quan)
    {
        if (quan>-1) return true;
        else return false;
    }

    /**
     *
     * @param id id clientului de updatat
     * @param type tipul de update email sau varsta
     * @param text si valoarea cu care trebuie updatat
     * @throws MyException se arunca o exceptie la interfata daca intrarea este invalida
     */
    public static void updateClient(String id ,String type,String text) throws MyException {
        try{
            Integer.parseInt(id);
        }
        catch (NumberFormatException e){
            throw new MyException("id");
        }

        if(type.equals("email") ) {
            if (emailValidator(text) == false) throw new MyException("email");
            if(WarehouseDAO.update("email","clients",text,id)==false)throw new MyException("id");
        }
        else {
            try {
                if (!AgeValidator(Integer.parseInt(text))) throw new MyException("age") ;

                if(!WarehouseDAO.update("age","clients",text,id))throw new MyException("id");

            } catch (NumberFormatException e) {
                throw new MyException("age");
            }
        }

    }

    /**
     *
     * @param id id ul produsului de updatat
     * @param quantity cantitatea produsului
     * @throws MyException se arunca o exceptie la interfata daca una dintre ele este eronata
     */
    public static void updateProduct(String id,String quantity) throws MyException {
        try{
            Integer.parseInt(id);
        }
        catch (NumberFormatException e){
            throw new MyException("id");
        }
        try {
            if (!quantityValidator(Integer.parseInt(quantity))) throw new MyException("quantity") ;

            if(!WarehouseDAO.update("quantity","products",quantity,id))throw new MyException("id");

        } catch (NumberFormatException e) {
            throw new MyException("quantity");
        }

    }

    /**
     *
     * @param email emailul de verificat
     * @return se returneaza true daca emailul este valid dupa un anumit pattern si false in caz contrar
     */
    private static boolean emailValidator(String email)
    {
        String emailRegex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
        Pattern emailPattern = Pattern.compile(emailRegex,Pattern.CASE_INSENSITIVE);
        Matcher matcher = emailPattern.matcher(email);
        return matcher.find();
    }

    /**
     *
     * @return Se returneaza toate produsele din baza de date
     */
    public static ArrayList<Product> getProducts() { return WarehouseDAO.selectAll("products"); }

    /**
     *
     * @param name Numele produsului de inserat
     * @param quantity cantiatea de inserat
     * @throws MyException arunca o exceptie daca cantiatea este eronata
     */
    public static void insertProduct(String name, String quantity) throws MyException {
        try {
            if (AgeValidator(Integer.parseInt(quantity)) == false) throw new MyException("quantity");
        }
        catch (NumberFormatException e)
        {
            throw new MyException("quantity");
        }
        ArrayList<String> a=new ArrayList<String>();a.add(name);a.add(quantity);
        WarehouseDAO.insert("products",a);
    }

    /**
     *
     * @param id id ul produslui de sters
     * @throws MyException arunca o exceptie daca id ul nu a fost gasit in baza de date
     */
    public static void deleteProduct(String id) throws MyException {
        try {
            Integer.parseInt(id);
            if(WarehouseDAO.delete(id,"products")==false) throw new MyException("id");
        }
        catch (NumberFormatException e)
        {
            throw new MyException("id");
        }
    }

    /**
     *
     * @param a se salveaza clientul logat in variabila instanta
     */
    public static void saveLoggedClient(Client a) { loggedClient=a; }

    /**
     *
     * @param a se salveaza produsele in lista de produse pentru orderul curent
     */
    public static void saveProduct(Product a){
        boolean found=false;
        for(Product product:products) {
            if(product.getIdproducts()==a.getIdproducts())
            {
                product.setQuantity(product.getQuantity()+a.getQuantity());
                found=true;
            }
        }
        if(found==false){products.add(a);}
    }

    /**
     *
     * @param a se salveaza produsului si cantitatea acestuia care la urma va fii updatata in baza de date
     */
    public static void updateQuantity(Product a){
        boolean found=false;
        for(Product product:updateQuantity) {
            if(product.getIdproducts()==a.getIdproducts())
            {
                product.setQuantity(product.getQuantity()+a.getQuantity());
                found=true;
            }
        }
        if(found==false){updateQuantity.add(a);}
    }

    /**
     *
     * @return se returneaza clientul logat
     */
    public static Client getLoggedClient(){ return loggedClient;}

    /**
     * Se goleste lista de produse la fiecare creare al unei comenzi noi
     */
    public static void clearProducts() {
        products.clear(); }

    /**
     * se goleste produsele care trebuie updatate in baza de date la fiecare creare al unei comenzi noi
     */
    public static void clearUpdateQuantity(){
        updateQuantity.clear();
    }

    /**
     *
     * @return se returneaza lista produselor de uptatat in baza de date
     */
    public static ArrayList<Product> getUpdateQuantity(){return updateQuantity;}

    /**
     *
     * @return returneaza lista produselor de tiparit in factura
     */
    public static ArrayList<Product> getChosenProducts(){return products;}


}
