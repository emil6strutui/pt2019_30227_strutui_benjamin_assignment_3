package Logic;

import Model.*;
import java.util.ArrayList;

public class OrderLogic {

    private Client client ;
    private Order order;
    private ArrayList<Product> products;

    public OrderLogic(Client client)
    {
        this.client=client;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }
}
