package Graphics;

import DAO.AbstractDAO;
import DAO.WarehouseDAO;
import Logic.OrderLogic;
import Logic.WarehouseLogic;
import Main.MyException;
import Model.Client;
import Model.Order;
import Model.Product;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * In aceasta clasa se implementeaza functionalitatea interfatei grafice
 */
public class GUIOperations extends GUI {

    /**
     * Constructorul care pune toate listenerele.
     */
    public GUIOperations() {
        allClientsLis();
        insertClientLis();
        goToClients();
        insertButton();
        deleteClientLis();
        deleteClient();
        backClientLis();
        updateClientLis();
        updateClient();
        goToProducts();
        backProductLis();
        allProductsLis();
        insertProduct();
        insertButonP();
        deleteProduct();
        deleteButonP();
        updateProduct();
        updateButonP();
        goToOrders();
        backOrderLis();
        orderLis();
        chooseClientLis();
        chooseProductsLis();
        finishOrder();
    }

    /**
     * Afisarea tuturor clientilor
     */
    private void allClientsLis(){
        getButon().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getScroll().setVisible(true);
                getPanel3().setVisible(false);
                getPanel4().setVisible(false);
                getPanel5().setVisible(false);
                getFrame1().validate();
                ArrayList<Client> client= WarehouseLogic.getClients();
                fillTable(client,"clients");
            }
        });
    }

    /**
     * Afisarea tuturor produselor
     */
    private void allProductsLis() {
        getListProducts().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getScroll2().setVisible(true);
                getProductPanel3().setVisible(false);
                getProductPanel4().setVisible(false);
                getProductPanel5().setVisible(false);
                getFrame2().validate();
                ArrayList<Product> products = WarehouseLogic.getProducts();
                fillTable(products,"products");
            }
        });
    }

    /**
     * Generarea interfetei pentru inserare
     */
    private void insertClientLis(){
           getInsert().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getScroll().setVisible(false);
                getPanel3().setVisible(true);
                getPanel4().setVisible(false);
                getPanel5().setVisible(false);
                getFrame1().validate();
            }
        });
    }

    /**
     * Inserarea in sine a datelor spre WarehouseLogic
     */
    private void insertButton() {
        getClientInsert().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    WarehouseLogic.insertClient(getName().getText(), getEmail().getText(), getAge().getText());
                    getPanel3().setVisible(false);
                    JOptionPane.showMessageDialog(null,"Insert successful","SUCCESS",JOptionPane.INFORMATION_MESSAGE);
                    getButon().doClick();
                } catch (MyException e1) {
                    if (e1.getMessage().equals("email")){
                        JOptionPane.showMessageDialog(null,"Wrong email!","Error",JOptionPane.ERROR_MESSAGE);
                        getEmail().setText("email");
                    }
                    if (e1.getMessage().equals("age"))
                    {
                        JOptionPane.showMessageDialog(null,"Wrong age!","Error",JOptionPane.ERROR_MESSAGE);
                        getAge().setText("age");
                    }
                }

            }
        });
    }

    /**
     * Deschide fereastra pentru editarea clientului
     */
    private void goToClients(){
        getClientsButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getFrame1().setVisible(true);
                getMainFrame().setVisible(false);
            }
        });
    }

    /**
     * Deschide fereastra pentru editarea produsului
     */
    private void goToProducts() {
        getProductsButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getFrame2().setVisible(true);
                getMainFrame().setVisible(false);
            }
        });
    }

    /**
     * Deschide fereastra pentru crearea orderului
     */

    private void goToOrders()
    {
        getOrderButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getFrame3().setVisible(true);
                getMainFrame().setVisible(false);
            }
        });
    }

    /**
     * Butonul back din fereastra produse care duce la Frame ul principal
     */
    private void backProductLis() {
        getBackProduct().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getMainFrame().setVisible(true);
                getFrame2().setVisible(false);
            }
        });
    }

    /**
     *
     * @param objects cu ce vom umple antetul si valorile
     * @param table variabila care spune ce fel de tabel trebuie umplut
     * Aceasta metoda umplem tabelul corespunzator care trebuie afisat pe ecran
     */
    private void fillTable(ArrayList objects,String table) {
        DefaultTableModel model;
        ArrayList columns=null;
        if (objects.size()!=0){ columns = AbstractDAO.retrieveProperties(objects.get(0));}
        model = new DefaultTableModel();
        if(columns!=null){ model.setColumnIdentifiers(columns.toArray());}

        if(table.equals("clients") || table.equals("ordersC")) {
            if(table.equals("clients")){getTable().setModel(model);}
            else {getTable3().setModel(model);}
            for (Client a : (ArrayList<Client>)objects) {
                Object[] row = new Object[4];
                row[0] = a.getId();
                row[1] = a.getName();
                row[2] = a.getEmail();
                row[3] = a.getAge();
                model.addRow(row);

            }
        }
        else if(table.equals("products") || table.equals("ordersP"))
        {
            if(table.equals("products")) {getTable2().setModel(model);}
            else {getTable3().setModel(model);}
            for (Product a : (ArrayList<Product>)objects) {
                Object[] row = new Object[3];
                row[0] = a.getIdproducts();
                row[1] = a.getName();
                row[2] = a.getQuantity();
                model.addRow(row);
            }
        }
    }

    /**
     * Pregateste fereastra pentru stergere
     */
    private void deleteClientLis() {
        getDeleteButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getButon().doClick();
                getPanel4().setVisible(true);
                getPanel3().setVisible(false);
                getPanel5().setVisible(false);
                getFrame1().validate();
            }
        });
    }

    /**
     * stergerea in sine a clientului
     */
    private void deleteClient() {
        getDeleteClient().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    WarehouseLogic.deleteClient(getDeleteId().getText());
                    JOptionPane.showMessageDialog(null,"Delete successful","SUCCESS",JOptionPane.INFORMATION_MESSAGE);
                    getButon().doClick();
                    getPanel4().setVisible(false);
                } catch (MyException e1) {
                    JOptionPane.showMessageDialog(null,"Wrong id!","Error",JOptionPane.ERROR_MESSAGE);
                    getDeleteId().setText("Id to be deleted");
                }
            }
        });
    }

    /**
     * Butonul back la frameul principal de la fereastra client
     */
    private void backClientLis() {
        getBackClient().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getMainFrame().setVisible(true);
                getFrame1().setVisible(false);
            }
        });
    }

    /**
     * pregateste fereastra pentru updatare in client
     */
    private void updateClientLis() {
        getUpdateButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getButon().doClick();
                getPanel5().setVisible(true);
                getPanel3().setVisible(false);
                getPanel4().setVisible(false);
                getFrame1().validate();
            }
        });
    }

    /**
     * updatarea clientului în sine și transmiterea datelor spre WarehouseLogic
     */
    private void updateClient() {
        getUpdateClient().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               try{
                   WarehouseLogic.updateClient(getUpdateId().getText(),(String)getUpdateBox().getSelectedItem(),getUpdateText().getText());
                   JOptionPane.showMessageDialog(null,"Update successful","SUCCESS",JOptionPane.INFORMATION_MESSAGE);
                   getButon().doClick();
                   getPanel5().setVisible(false);
               } catch (MyException e1) {
                   JOptionPane.showMessageDialog(null,"Wrong " +e1.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
                   if (e1.getMessage().equals("id")) {
                       getUpdateId().setText("Id to be updated");
                   }
                    else if   (e1.getMessage().equals("email"))
                       {
                           getUpdateText().setText("");
                       }
                    else getUpdateText().setText("");
               }
            }
        });
    }

    /**
     * pregateste fereastra product pentru inserare
     */
    private void insertProduct()
    {
        getInsertProduct().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getScroll2().setVisible(false);
                getProductPanel3().setVisible(true);
                getProductPanel4().setVisible(false);
                getProductPanel5().setVisible(false);
                getFrame2().validate();
            }
        });

    }

    /**
     * Inserarea in sine de produs
     */
    private void insertButonP()
    {
        getInsertP().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    WarehouseLogic.insertProduct(getProductName().getText(), getProductQuantity().getText());
                    JOptionPane.showMessageDialog(null,"Insert successful","SUCCESS",JOptionPane.INFORMATION_MESSAGE);
                    getListProducts().doClick();
                } catch (MyException e1) {

                    if (e1.getMessage().equals("quantity"))
                    {
                        JOptionPane.showMessageDialog(null,"Wrong quantity!","Error",JOptionPane.ERROR_MESSAGE);
                        getProductQuantity().setText("quantity");
                    }
                }
            }
        });
    }

    /**
     * pregatirea ferestrei pentru stergere
     */
    private void deleteProduct()
    {
        getDeleteButtonP().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getListProducts().doClick();
                getProductPanel4().setVisible(true);
                getFrame2().validate();
            }
        });
    }

    /**
     * Stergerea produsului in sine
     */
    private void deleteButonP(){
        getDeleteProductB().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    WarehouseLogic.deleteProduct(getDeleteProduct().getText());
                    JOptionPane.showMessageDialog(null,"Delete successful","SUCCESS",JOptionPane.INFORMATION_MESSAGE);
                    getListProducts().doClick();
                    getProductPanel4().setVisible(false);
                } catch (MyException e1) {
                    JOptionPane.showMessageDialog(null,"Wrong id!","Error",JOptionPane.ERROR_MESSAGE);
                    getDeleteProduct().setText("Id to be deleted");
                }
            }
        });
    }

    /**
     * pregatirea ferestrei produs pentru update
     */
    private void updateProduct()
    {
        getUpdateButtonP().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getListProducts().doClick();
                getProductPanel5().setVisible(true);
                getFrame2().validate();
            }
        });
    }

    /**
     * updateul produsului in sine
     */
    private void updateButonP()
    {
        getUpdateProductB().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    WarehouseLogic.updateProduct(getUpdateProduct().getText(),getUpdateQuantity().getText());
                    JOptionPane.showMessageDialog(null,"Update successful","SUCCESS",JOptionPane.INFORMATION_MESSAGE);
                    getListProducts().doClick();
                    getProductPanel5().setVisible(false);
                } catch (MyException e1) {
                    JOptionPane.showMessageDialog(null,"Wrong " +e1.getMessage()+"!","Error",JOptionPane.ERROR_MESSAGE);
                    if (e1.getMessage().equals("id"))
                        getUpdateProduct().setText("Id to be updated");
                    else getUpdateQuantity().setText("Quantity");
                }
            }
        });
    }

    /**
     *   Butonul back pentru order la frame ul principal
     */
    private void backOrderLis()
    {
        getBackOrder().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getMainFrame().setVisible(true);
                getFrame3().setVisible(false);
            }
        });
    }

    /**
     *  pregatirea ferestrei pentru alegerea clientului pentru care se face order
     */
    private void orderLis()
    {
        getCreateOrder().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getScroll3().setVisible(true);
                getChooseProducts().setVisible(true);
                getOrderClientid().setVisible(true);
                getOrderProductid().setVisible(false);
                getOrderProductQuantity().setVisible(false);
                getConfirmProduct().setVisible(false);
                getFinishCommand().setVisible(false);
                getFrame3().validate();
                ArrayList<Client> client= WarehouseLogic.getClients();
                fillTable(client,"ordersC");
            }
        });
    }

    /**
     * Alegerea clientului și pregatirea ferestrei pentru alegerea produselor
     */
    private void chooseClientLis()
    {
        getChooseProducts().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client a=null;
                WarehouseLogic.clearProducts();
                WarehouseLogic.clearUpdateQuantity();
                try {
                    Integer.parseInt(getOrderClientid().getText());
                    for(int i=0;i<getTable3().getModel().getRowCount();i++)
                    {
                       if(Integer.parseInt(getOrderClientid().getText())==(Integer)getTable3().getModel().getValueAt(i,0))
                       {
                           a=new Client((Integer)getTable3().getModel().getValueAt(i,0),(String)getTable3().getModel().getValueAt(i,1)
                           ,(String)getTable3().getModel().getValueAt(i,2),(Integer)getTable3().getModel().getValueAt(i,3));

                       }
                    }
                    if (a!=null) {WarehouseLogic.saveLoggedClient(a);
                        getChooseProducts().setVisible(false);
                        getOrderClientid().setVisible(false);
                        ArrayList<Product> products = WarehouseLogic.getProducts();
                        fillTable(products,"ordersP");
                        getOrderProductid().setVisible(true);
                        getOrderProductQuantity().setVisible(true);
                        getConfirmProduct().setVisible(true);
                        getFinishCommand().setVisible(true);
                    }
                    else{JOptionPane.showMessageDialog(null,"Wrong id","Error",JOptionPane.ERROR_MESSAGE);
                        getOrderClientid().setText("Order client id!");

                    }
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(null,"Wrong id","Error",JOptionPane.ERROR_MESSAGE);
                    getOrderClientid().setText("Order client id!");
                }

            }
        });
    }

    /**
     * butonul care introduce produsele noastre intr-o lista
     */
    private void chooseProductsLis()
    {
        getConfirmProduct().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Product a=null;
                boolean foundId =false;
                boolean foundProd =false;
                try {
                    Integer.parseInt(getOrderProductid().getText());
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(null,"Wrong id","Error",JOptionPane.ERROR_MESSAGE);
                    getOrderProductid().setText("Order product id!");
                }

                try {
                    Integer.parseInt(getOrderProductQuantity().getText());
                    for(int i=0;i<getTable3().getModel().getRowCount();i++) {

                        if (Integer.parseInt(getOrderProductid().getText()) ==  (Integer)getTable3().getModel().getValueAt(i, 0)) {
                            foundId=true;
                            if((Integer) getTable3().getModel().getValueAt(i, 2) - Integer.parseInt(getOrderProductQuantity().getText())>=0 && Integer.parseInt(getOrderProductQuantity().getText())>0 )
                            {
                                foundProd=true;
                                a=new Product((Integer)getTable3().getModel().getValueAt(i,0),(String)getTable3().getModel().getValueAt(i,1),Integer.parseInt(getOrderProductQuantity().getText()));
                                WarehouseLogic.saveProduct(a);
                                a=new Product((Integer)getTable3().getModel().getValueAt(i,0),(String)getTable3().getModel().getValueAt(i,1),(Integer) getTable3().getModel().getValueAt(i, 2) - Integer.parseInt(getOrderProductQuantity().getText()));
                                WarehouseLogic.updateQuantity(a);
                                getTable3().getModel().setValueAt( (Integer) getTable3().getModel().getValueAt(i, 2) - Integer.parseInt(getOrderProductQuantity().getText()),i,2);
                            }
                        }
                    }
                    if(foundId==false)
                    {
                        JOptionPane.showMessageDialog(null,"Wrong id","Error",JOptionPane.ERROR_MESSAGE);
                        getOrderProductid().setText("Order product id!");
                    }
                    else if (foundProd==false)
                    {
                        JOptionPane.showMessageDialog(null,"Wrong quantity","Error",JOptionPane.ERROR_MESSAGE);
                        getOrderProductQuantity().setText("Order product quantity!");
                    }
                    else{
                        JOptionPane.showMessageDialog(null,"Product Added","SUCCES",JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(null,"Wrong quantity","Error",JOptionPane.ERROR_MESSAGE);
                    getOrderProductQuantity().setText("Order product quantity!");
                }

            }
        });
    }

    /**
     * listener la butonul care finalizeaza comanda trimite datele necesare la baza de date si creeaza documentul PDF
     */
    private void finishOrder()
    {
        getFinishCommand().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (WarehouseLogic.getChosenProducts().size()>0) {
                    OrderLogic order = new OrderLogic(WarehouseLogic.getLoggedClient());
                    order.setProducts(WarehouseLogic.getChosenProducts());
                    order.setOrder(new Order(WarehouseDAO.getMaxId("idorders", "orders") + 1, "Order Number:" + (WarehouseDAO.getMaxId("idorders", "orders") + 1)));
                    WarehouseLogic.insertOrder();
                    for(Product product:WarehouseLogic.getUpdateQuantity())
                    {
                        try {
                            WarehouseLogic.updateProduct(String.valueOf(product.getIdproducts()),String.valueOf(product.getQuantity()));
                            WarehouseLogic.insertOrderRecord(String.valueOf(WarehouseLogic.getLoggedClient().getId()),String.valueOf(product.getIdproducts()),String.valueOf(product.getQuantity()));
                        } catch (MyException e1) {
                            e1.printStackTrace();
                        }
                    }
                    Document document=new Document();
                    try{
                        PdfWriter.getInstance(document,new FileOutputStream("Order " + (WarehouseDAO.getMaxId("idorders", "orders"))+".pdf"));
                        document.open();
                        document.add(new Paragraph("Order with the order number of:"+(WarehouseDAO.getMaxId("idorders", "orders"))));
                        document.add(new Paragraph("Order made by:"+WarehouseLogic.getLoggedClient().getName()+" with email:"+WarehouseLogic.getLoggedClient().getEmail()+" and age:"+WarehouseLogic.getLoggedClient().getAge()));
                        document.add(new Paragraph(" "));

                        PdfPTable tb1=new PdfPTable(2);
                        tb1.addCell("Product Name");
                        tb1.addCell("Product Quantity");
                        for(Product a:WarehouseLogic.getChosenProducts())
                        {
                            tb1.addCell(a.getName());
                            tb1.addCell(String.valueOf(a.getQuantity()));
                        }
                        document.add(tb1);
                        JOptionPane.showMessageDialog(null,"Order finished\n The file Order " + (WarehouseDAO.getMaxId("idorders", "orders"))+".pdf was created","SUCCES",JOptionPane.INFORMATION_MESSAGE);


                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    } catch (DocumentException e1) {
                        e1.printStackTrace();
                    }
                    finally {
                        document.close();
                    }

                }




            }
        });
    }

}



