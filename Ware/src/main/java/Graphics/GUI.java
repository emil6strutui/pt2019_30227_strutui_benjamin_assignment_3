package Graphics;
import javax.swing.*;
import java.awt.*;
public class GUI {

    //---------------------------------------------------------Main
    private JFrame mainFrame = new JFrame();
    private JPanel mainPanel = new JPanel();
    private JButton clientsButton = new JButton("Clients");
    private JButton productsButton = new JButton("Products");
    private JButton orderButton = new JButton("Create Order");
    //-----------------------------------------------------------Clients
    private JFrame frame1 = new JFrame();
    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JPanel panel3 = new JPanel();
    private JPanel panel4 = new JPanel();

    private JPanel panel5 = new JPanel();
    private JTextField updateId = new JTextField("Id to be updated");
    private String[ ] comboStrings ={"email","age"};
    private JComboBox updateBox = new JComboBox(comboStrings);
    private JTextField updateText = new JTextField("");
    private JButton updateClient = new JButton("Update");
    private JTextField deleteId = new JTextField("Id to be deleted");
    private JButton deleteButton= new JButton("Delete Client");

    private JButton updateButton = new JButton("UpdateClient");

    private JButton backClient = new JButton("Back");

    private JButton deleteClient = new JButton("Delete");
    private JTextField name = new JTextField("Name");
    private JTextField email = new JTextField("Email");
    private JTextField age = new JTextField("Age");
    private JButton clientInsert = new JButton("Insert");
    private JTable table = new JTable();
    private JButton querry = new JButton("View Clients");
    private JButton insert = new JButton("Insert Client");
    private JScrollPane scroll ;

    //---------------------------------------------------------------Products
    private JFrame frame2 = new JFrame();
    private JPanel productPanel1 = new JPanel();
    private JButton listProducts = new JButton("View Products");
    private JButton insertProduct = new JButton("Insert Product");
    private JButton deleteButtonP= new JButton("Delete Product");
    private JButton updateButtonP = new JButton("Update Product");
    private JButton backProduct = new JButton("Back");

    private JPanel productPanel2 = new JPanel();
    private JScrollPane scroll2 ;
    private JTable table2 = new JTable();


    private JPanel productPanel3 = new JPanel();
    private JTextField productName = new JTextField("Product Name");
    private JTextField productQuantity = new JTextField("Product Quantity");
    private JButton insertP = new JButton("Insert");

    private JPanel productPanel4 = new JPanel();
    private JTextField deleteProduct = new JTextField("Product id to deleted");
    private JButton deleteProductB = new JButton("Delete");

    private JPanel productPanel5 = new JPanel();
    private JTextField updateProduct = new JTextField("Product id to update");
    private JTextField updateQuantity = new JTextField("Quantity");
    private JButton updateProductB = new JButton("Update");

    //--------------------------------------------------------------Orders
    private JFrame frame3 = new JFrame();
    private JPanel orderPanel1 = new JPanel();
    private JPanel orderPanel2 = new JPanel();
    private JButton createOrder = new JButton("Order");
    private JButton backOrder = new JButton("Back");
    private JScrollPane scroll3 ;
    private JTable table3 = new JTable();
    private JTextField orderClientid = new JTextField("choose client id");
    private JButton  chooseProducts = new JButton("Apply");
    private JTextField orderProductid= new JTextField("Choose product id");
    private JTextField orderProductQuantity= new JTextField("Choose product quantity");
    private JButton confirmProduct = new JButton("Add Product");
    private JButton finishCommand = new JButton("Finish Order");


    //--------------------------------------------------------------

    public GUI()
    {
        //Main Frame
        mainPanel.setLayout(new FlowLayout(0));
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(300,80);
        mainFrame.setResizable(false);
        mainPanel.add(clientsButton);
        mainPanel.add(productsButton);
        mainPanel.add(orderButton);
        mainFrame.add(mainPanel);
        mainFrame.setVisible(true);
        //------------------------------------------------------------


        //Clients Frame
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame1.setLocationRelativeTo(null);
        frame1.setSize(600, 500);
        frame1.setResizable(false);
        panel1.setLayout(new FlowLayout(0));

        panel2.setLayout(new FlowLayout());

        panel3.setLayout(new FlowLayout());
        name.setPreferredSize(new Dimension(150,20));
        email.setPreferredSize(new Dimension(150,20));
        panel3.add(name);
        panel3.add(email);
        panel3.add(age);
        panel3.add(clientInsert);

        panel4.setLayout(new FlowLayout());
        panel4.add(deleteId);
        panel4.add(deleteClient);
        deleteId.setPreferredSize(new Dimension(100,20));


        panel5.setLayout(new FlowLayout());
        panel5.add(updateId);
        panel5.add(updateBox);
        panel5.add(updateText);
        panel5.add(updateClient);
        updateId.setPreferredSize(new Dimension(100,20));
        updateText.setPreferredSize(new Dimension(120,20));
        scroll=new JScrollPane(table);
        scroll.setPreferredSize(new Dimension(550,200));
        scroll.setVisible(false);

        panel1.add(querry);
        panel1.add(insert);
        panel1.add(deleteButton);
        panel1.add(updateButton);
        panel1.add(backClient);
        panel2.add(scroll);

        panel2.add(panel3);
        panel2.add(panel4);
        panel2.add(panel5);

        panel3.setVisible(false);
        panel4.setVisible(false);
        panel5.setVisible(false);

        frame1.add(panel1,BorderLayout.NORTH);
        frame1.add(panel2,BorderLayout.CENTER);
        frame1.setVisible(false);
        //---------------------------------------------------------------

        //Product frame
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame2.setLocationRelativeTo(null);
        frame2.setSize(600, 500);
        frame2.setResizable(false);
        productPanel1.setLayout(new FlowLayout(0));
        productPanel1.add(listProducts);
        productPanel1.add(insertProduct);
        productPanel1.add(deleteButtonP);
        productPanel1.add(updateButtonP);
        productPanel1.add(backProduct);

        scroll2=new JScrollPane(table2);
        productPanel2.add(scroll2);
        scroll2.setPreferredSize(new Dimension(550,200));
        scroll2.setVisible(false);

        productPanel3.setLayout(new FlowLayout());
        productPanel3.add(productName);
        productPanel3.add(productQuantity);
        productPanel3.add(insertP);
        productName.setPreferredSize(new Dimension(100,20));
        productQuantity.setPreferredSize(new Dimension(100,20));
        productPanel3.setVisible(false);
        productPanel2.add(productPanel3);

        productPanel4.setLayout(new FlowLayout());
        productPanel4.add(deleteProduct);
        productPanel4.add(deleteProductB);
        deleteProduct.setPreferredSize(new Dimension(120,20));
        productPanel2.add(productPanel4);
        productPanel4.setVisible(false);


        productPanel5.setLayout(new FlowLayout());
        productPanel5.add(updateProduct);
        productPanel5.add(updateQuantity);
        productPanel5.add(updateProductB);
        updateProduct.setPreferredSize(new Dimension(120,20));
        updateQuantity.setPreferredSize(new Dimension(60,20));
        productPanel2.add(productPanel5);
        productPanel5.setVisible(false);

        frame2.add(productPanel1,BorderLayout.NORTH);
        frame2.add(productPanel2,BorderLayout.CENTER);
        frame2.setVisible(false);
        //---------------------------------------------------------------Order Frame
        frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame3.setLocationRelativeTo(null);
        frame3.setSize(600, 500);
        frame3.setResizable(false);
        orderPanel1.setLayout(new FlowLayout(0));
        orderPanel1.add(createOrder);
        orderPanel1.add(backOrder);


        scroll3=new JScrollPane(table3);
        orderPanel2.add(scroll3);
        scroll3.setPreferredSize(new Dimension(550,200));
        scroll3.setVisible(false);
        orderPanel2.add(orderClientid);
        orderClientid.setPreferredSize(new Dimension(120,20));
        orderClientid.setVisible(false);
        orderPanel2.add(chooseProducts);
        chooseProducts.setVisible(false);
        orderPanel2.add(orderProductid);
        orderProductid.setPreferredSize(new Dimension(120,20));
        orderPanel2.add(orderProductQuantity);
        orderProductQuantity.setPreferredSize(new Dimension(130,20));
        orderPanel2.add(confirmProduct);
        orderPanel2.add(finishCommand);
        orderProductid.setVisible(false);
        orderProductQuantity.setVisible(false);
        confirmProduct.setVisible(false);
        finishCommand.setVisible(false);

        frame3.add(orderPanel1,BorderLayout.NORTH);
        frame3.add(orderPanel2,BorderLayout.CENTER);
        frame3.setVisible(false);


        //---------------------------------------------------------------
    }

    public JButton getButon() {
        return querry;
    }

    public JTable getTable() {
        return table;
    }

    public JButton getInsert() {
        return insert;
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    public JButton getClientsButton() {
        return clientsButton;
    }

    public JButton getProductsButton() {
        return productsButton;
    }

    public JButton getOrderButton() {
        return orderButton;
    }

    public JFrame getFrame1() {
        return frame1;
    }

    public JScrollPane getScroll() {
        return scroll;
    }

    public JPanel getPanel3() {
        return panel3;
    }

    public JTextField getName() {
        return name;
    }

    public JTextField getEmail() {
        return email;
    }

    public JTextField getAge() {
        return age;
    }

    public JButton getClientInsert() {
        return clientInsert;
    }

    public JTextField getDeleteId() {
        return deleteId;
    }

    public JButton getDeleteClient() {
        return deleteClient;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public JPanel getPanel4() {
        return panel4;
    }

    public JButton getBackClient() {
        return backClient;
    }

    public JButton getUpdateButton() {
        return updateButton;
    }

    public JPanel getPanel5() {
        return panel5;
    }

    public JComboBox getUpdateBox() {
        return updateBox;
    }

    public JTextField getUpdateText() {
        return updateText;
    }

    public JButton getUpdateClient() {
        return updateClient;
    }

    public JTextField getUpdateId() {
        return updateId;
    }

    public JFrame getFrame2() {
        return frame2;
    }

    public JButton getListProducts() {
        return listProducts;
    }

    public JButton getInsertProduct() {
        return insertProduct;
    }

    public JButton getDeleteButtonP() {
        return deleteButtonP;
    }

    public JButton getUpdateButtonP() {
        return updateButtonP;
    }

    public JButton getBackProduct() {
        return backProduct;
    }


    public JPanel getProductPanel1() {
        return productPanel1;
    }

    public JPanel getProductPanel2() {
        return productPanel2;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JPanel getProductPanel3() {
        return productPanel3;
    }

    public JTextField getProductName() {
        return productName;
    }

    public JTextField getProductQuantity() {
        return productQuantity;
    }

    public JButton getInsertP() {
        return insertP;
    }

    public JScrollPane getScroll2() {
        return scroll2;
    }

    public JTable getTable2() {
        return table2;
    }

    public JTextField getDeleteProduct() {
        return deleteProduct;
    }

    public JButton getDeleteProductB() {
        return deleteProductB;
    }

    public JPanel getProductPanel4() {
        return productPanel4;
    }

    public JPanel getProductPanel5() {
        return productPanel5;
    }

    public JTextField getUpdateProduct() {
        return updateProduct;
    }

    public JTextField getUpdateQuantity() {
        return updateQuantity;
    }

    public JButton getUpdateProductB() {
        return updateProductB;
    }

    public JFrame getFrame3() {
        return frame3;
    }

    public JPanel getOrderPanel1() {
        return orderPanel1;
    }

    public JPanel getOrderPanel2() {
        return orderPanel2;
    }

    public JButton getCreateOrder() {
        return createOrder;
    }

    public JScrollPane getScroll3() {
        return scroll3;
    }

    public JTable getTable3() {
        return table3;
    }

    public JButton getBackOrder() {
        return backOrder;
    }

    public JTextField getOrderClientid() {
        return orderClientid;
    }

    public JButton getChooseProducts() {
        return chooseProducts;
    }

    public JTextField getOrderProductid() {
        return orderProductid;
    }

    public JTextField getOrderProductQuantity() {
        return orderProductQuantity;
    }

    public JButton getConfirmProduct() {
        return confirmProduct;
    }

    public JButton getFinishCommand() {
        return finishCommand;
    }
}
