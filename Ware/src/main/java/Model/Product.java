package Model;

public class Product {
    private Integer idproducts;
    private String name;
    private Integer quantity;


    public Product(Integer id, String name,Integer quantity) {
        this.idproducts = id;
        this.name = name;
        this.quantity=quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdproducts() {
        return idproducts;
    }

    public void setIdproducts(Integer idproducts) {
        this.idproducts = idproducts;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                ", quantity=" + quantity +
                "\n";
    }
}

