package Model;

public class Client {
    private Integer idclients;
    private String name;
    private String email;
    private Integer age;



    public Client(Integer id,String name,String email,Integer age)
    {
        this.idclients=id;
        this.name=name;
        this.email=email;
        this.age=age;
    }

    public Integer getId() {
        return idclients;
    }

    public void setId(Integer id) {
        this.idclients = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Client{" +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                '}';
    }
}
