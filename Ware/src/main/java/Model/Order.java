package Model;

public class Order {
    private Integer idorders;
    private String name;


    public Order(Integer id,String name)
    {
        this.idorders=id;
        this.name=name;
    }

    public Integer getId() {
        return idorders;
    }

    public void setId(Integer id) {
        this.idorders = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Order{" +
                "idorders=" + idorders +
                ", name='" + name + '\'' +
                '}';
    }
}
