package Connection;

import java.sql.*;
import java.util.logging.Logger;

/**
 * Aceasta clasa realizeaza conexiunea la baza de date
 */
public final class ConnectionFactory {
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/warehouse?useSSL=false";
    private static final String USER = "root";
    private static final String PASS = "leagueofl619";
    private static ConnectionFactory singleInstance = new ConnectionFactory();

    /**
     * initializarea variabile singelton al clasei
     */
    private ConnectionFactory()
    {
        try{
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return returneaza o coexiune la baza de date care va fii stocata in variabila singleInstance
     */
    private Connection createConnection()
    {
        try {
             return DriverManager.getConnection(DBURL, USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @return se returneaza conexiunea pentru WarehouseDAO
     */
    public static Connection getConnection()
    {
        return singleInstance.createConnection();
    }

    /**
     *
     * @param connection inchiderea conexiunii
     */
    public static void close(Connection connection)
    {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param statement inchiderea statementului
     */
    public static void close(Statement statement)
    {   try {
        statement.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    }

    /**
     *
     * @param resultSet inchiderea resultSet ului
     */
    public static void close(ResultSet resultSet)
    {
        try {
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
