package DAO;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;

public class AbstractDAO<T> {
    private final Class<T> type;


    @SuppressWarnings("unchecked")
    public AbstractDAO()
    {
        this.type = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     *
     * @param object este un obiect la care vor fii preluate fieldurile pentru completarea antetului unui tabel pt interfata grafica
     * @return se returneaza o lista de obiecte string care va fii folosit pentru umplerea antetului tabelului de afisat
     */
    public static ArrayList<String> retrieveProperties(Object object)
    {   ArrayList<String> columns = new ArrayList<String>();
        for(Field field:object.getClass().getDeclaredFields())
        {
            field.setAccessible(true);
            Object value;
            try {
                value=field.get(object);
                //System.out.println(field.getName()+"="+value);
                columns.add(field.getName());

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return columns;
    }


}
