package DAO;
import Connection.ConnectionFactory;
import Model.Client;
import Model.Product;

import java.sql.*;
import java.util.ArrayList;

/**
 * In aceasta clasa se face comunicarea directa cu baza de date si executarea querryurilor
 */
public class WarehouseDAO {

    /**
     *
     * @param table tabelul de unde dorim sa selectam
     * @return returneaza o lista de produse corespunzatoare tabelului din care dorim sa selectam
     */
    public static ArrayList selectAll(String table) {   Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList objects=null ;
        try {
            con= ConnectionFactory.getConnection();
            st=con.prepareStatement("Select  * from "+table);
            rs=st.executeQuery();
            if(table.equals("clients")){
                objects=new ArrayList<Client>();
            while(rs.next()) {
                objects.add(new Client(rs.getInt("idclients"), rs.getString("name"), rs.getString("email"), Integer.parseInt(rs.getString("age"))));
            }
            }
            else if(table.equals("products"))
            {    objects=new ArrayList<Product>();
                while(rs.next()) {
                    objects.add(new Product(rs.getInt("idproducts"), rs.getString("name"), Integer.parseInt(rs.getString("quantity"))));
                }

                }

            return objects;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        finally {
            closeConnections(con,st,rs);
        }
    }

    /**
     *
     * @param id id ul obiectului care dorim sa stergem
     * @param table tabelul de unde dorim sa stergem
     * @return true in caz de success si false in caz contrar
     */
    public static boolean delete(String id,String table) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            con = ConnectionFactory.getConnection();
            if(table.equals("clients")){ st = con.prepareStatement("delete from "+table+" where id"+table+"=?" );}
            else if (table.equals("products")){ st = con.prepareStatement("delete from "+table+" where id"+table+"=?" );}
            else if (table.equals("orders")){ st = con.prepareStatement("delete from "+table+" where id"+table+"=?" );}
            st.setInt(1,Integer.parseInt(id));
            int delete=st.executeUpdate();
            if (delete==0) return false;
        } catch (SQLException e) {
            return false;
        }
        finally {
            closeConnections(con,st,rs);
        }

        return true;
    }

    /**
     *
     * @param field fieldul care dorim sa fie updatat
     * @param table tabela la care dorim sa realizam update ul
     * @return se returneaza un querry pentru executie
     */
    private static String createUpdate(String field,String table) {
        StringBuilder sb = new StringBuilder();
        sb.append("update ");sb.append(table);sb.append(" set ");sb.append( field  );sb.append("=?");sb.append(" Where " + "id"+table +" =?");
        return sb.toString();
    }

    /**
     *
     * @param field fieldul de updatat
     * @param table tabelul de updatat
     * @param text continutul cu care updatam
     * @param id id ul la cine updatam
     * @return true in caz de succes false altfel
     */
    public static boolean update(String field,String table,String text,String id) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            con = ConnectionFactory.getConnection();
            st = con.prepareStatement(createUpdate(field,table));
            st.setInt(2,Integer.parseInt(id));
            st.setString(1,text);
            int update=st.executeUpdate();
            if (update==0) return false;
        } catch (SQLException e) {
            return false;
        }
        finally {
            closeConnections(con,st,rs);
        }

        return true;
    }

    /**
     *
     * @param size variabila descrie cat de multe obiecte vor fii de inserat in tabel
     * @param table tabelul la care inseram
     * @return returneaza querry ul
     */
    private static String createInsert(Integer size,String table) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into ");sb.append(table);sb.append(" values(?");
        for(int i=0;i<size;i++)
            sb.append(",?");
        sb.append(")");
        return sb.toString();
    }

    /**
     *
     * @param table tabelul in care se insereazza
     * @param toInsert continutul de inserat in tabel
     */
    public static void insert(String table,ArrayList<String>toInsert) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            con = ConnectionFactory.getConnection();
            st = con.prepareStatement(createInsert(toInsert.size(),table));
            if(table.equals("clients")) {
                st.setInt(1, getMaxId("idclients", "clients") + 1); }
            else if(table.equals("products")){    st.setInt(1, getMaxId("idproducts", "products") + 1);}
            else if(table.equals("orders")){    st.setInt(1, getMaxId("idorders", "orders") + 1);}
            else if(table.equals("order_record")) {    st.setInt(1, getMaxId("idorders", "orders"));}
            for(int i=0;i<toInsert.size();i++) {
                st.setString(i+2, toInsert.get(i));
            }
                st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            closeConnections(con,st,rs);
        }
    }

    /**
     *
     * @param field fieldul cu numele id ului corespunzator
     * @param table tabela de unde dorim sa preluam id ul maxim
     * @return se returneaza un id maxim +1
     */
    public static Integer getMaxId(String field,String table) {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            con = ConnectionFactory.getConnection();
            st = con.createStatement();
            rs = st.executeQuery("select max("+field+") from "+table);
            rs.next();
            return rs.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            closeConnections(con,st,rs);
        }
    }

    /**
     *
     * @param con Conexiunea
     * @param st Statementul
     * @param rs Result Set ul
     *           In aceasta metoda dupa ce se face querry ul se inchid toate este obiecte
     */
    private static void closeConnections(Connection con ,Statement st,ResultSet rs) {
        if(rs!=null) {
            ConnectionFactory.close(rs);
        }
        if(st!=null){
            ConnectionFactory.close(st);
        }
        if(con!=null) {
            ConnectionFactory.close(con);
        }
    }

}
